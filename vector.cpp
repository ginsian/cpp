#include <initializer_list>
#include <algorithm>
#include "vector.hpp"
using namespace std; 


Vector::Vector (initializer_list<double> lst)
	:elem{new double[lst.size()]},sz{static_cast<int>(lst.size())}
	{
		copy(lst.begin(),lst.end(),elem);
	}
Vector::Vector (int s):sz{s}, elem { new double[s]}{
		for (int i=0;i!=s;i++) elem[i]=0;

}
double& Vector::operator[](int s) {
	return elem[s];
}
int Vector::size()const{
	return sz;
}
