#include <list>
#include "container.hpp"

using namespace std;
class List_container: public Container {
    list<double> ld;
    public:
        List_container();
        List_container(initializer_list<double> il);
        ~List_container();
        double & operator[](int i);
        int size()const;

};
