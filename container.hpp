#ifndef CONTAINER_H
#define CONTAINER_H

class Container {

public:
virtual double& operator[](int) =0;    //purevirtual
	virtual int size() const=0;   // const member function
	virtual ~Container(){}		// Destructoyntax on
};
#endif
