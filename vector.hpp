#ifndef VECTOR_HPP
#define VECTOR_HPP
#include <initializer_list>
using namespace std;
class Vector {
	public:
	//
	Vector (int s);

	// Initializer constructor
	//
    //MyNumber(const std::initializer_list<int> &v) {
	Vector (initializer_list<double> );

	~Vector() {
		delete [] elem;
	}

	double &  operator[](int);
	int size() const;

	private:
	    double *elem;
	    int sz;

};
#endif
