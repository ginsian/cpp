#include <list>
#include <stdexcept>
#include "container.hpp"
#include "list_container.hpp"
#include <initializer_list>
using namespace std;

List_container::List_container(){};

List_container::List_container(initializer_list<double> il):ld{il}{}

List_container::~List_container(){}

int List_container::size()const { return ld.size();}

double & List_container::operator[](int i)  {
    for (auto& x:ld){
        if (i==0) return x;
        i--;
    }
    throw out_of_range("List_out_of_range");

}
