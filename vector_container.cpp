#include "vector_container.hpp"
Vector_container::Vector_container(int s) : v(s){}   // Vector of s elements

Vector_container::Vector_container(initializer_list<double> list):v(list){ }

Vector_container::~Vector_container() {}

double & Vector_container::operator[](int i){return v[i];}
int Vector_container:: size() const { return v.size();}


