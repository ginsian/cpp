#include <iostream>
#include "vector_container.hpp"
#include "vector.hpp"
#include "container.hpp"
#include "list_container.hpp"
using namespace std;
void use (Container &c) {
    const int sz = c.size();
    for (int i=0;i!=sz;i++){
        cout << c[i] << "\n";
    };
}
void h(){
    List_container lc=  {10,9,8, 7,6,5,4,3,2,1};
    use(lc);
}
void g(){
    cout<<"Called in g\n";
    Vector_container vc = {10,9,8, 7,6,5,4,3,2,1};
    use(vc);
}
int main (){
    cout<< "Hello\n";
   g();
   h();
    cout<< "World\n";
return 0;
}
