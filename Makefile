CC = g++
SRCS = vector_container.cpp \
	   vector.cpp 
#list_container.cpp

OBJS = vector_container.o \
	   vector.o \
	   list_container.o \
	   main.o
#list_container.o

OUTFILE = a.out
DEBUG = -g
CFLAGS = -std=c++11  -Wall -c $(DEBUG) -Wno-c++11-narrowing 
#CFLAGS = -std=c++11  -c  $(DEBUG)

$(OUTFILE)	:  main.cpp $(OBJS)
	$(CC) -o $(OUTFILE)   $(OBJS) 
main.o	: main.cpp
	$(CC) $(CFLAGS) main.cpp
vector_container.o	: vector_container.cpp vector_container.hpp
	$(CC) $(CFLAGS) vector_container.cpp
vector.o 	:vector.hpp  vector.cpp
	$(CC) $(CFLAGS)  vector.cpp
list_container.o 	: list_container.cpp list_container.hpp
	$(CC) $(CFLAGS) list_container.cpp
clean:	
	rm *.o
	rm $(OUTFILE)
